## Install

- Clone repo
- Run composer -> composer install
- Set up Env -> .env
- php artisan migrate
- php artisan db:seed
- npm install
- npm run dev
- Create folder uploads (mistake, deleted from git) so -> /public/uploads  

After seed you will have a few users but two of them will be admininstrators:
Email: figured@test.com
Password: secret

Email: ezequiel@test.com
Password: secret


## Details

Laravel 5.8
Vue.js - vuex
Mysql (Users)
MongoDB (Posts)

## Admin area
/admin

--------------


